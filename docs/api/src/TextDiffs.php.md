<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/TextDiffs.php  
  
# class DecaturVote\Differ  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function fix_html(string $html): string` Convert $html into a beautified version with line breaks. Uses taeluf/phtml v0.1.x-dev, which uses DOMDocument  
  
- `public function forward(string $old_text, string $opps)` Convert $old_text into NEW TEXT by applying $opps  
  
- `public function backward(string $new_text, string $opps)` Convert $new_text into OLD TEXT by un-applying $opps (essentially runs the opps backward)  
  
- `public function get_opps(string $old, string $new): string` Get a string of operations to convert from $old to $new   
Allows backward conversions, too.  
  
- `protected function add_line(array $lines, int $line_number, string $new_line): array` Add a line to the line buffer & return the corrected lines  
  
- `protected function remove_line(array $lines, int $line_number): array` Remove a line from the buffer & return the corrected lines array  
  
- `protected function move_lines(array $lines, array $move_opps)` Apply move operations to an array of lines  
  
- `protected function parse_opp_line(string $opp_line, int $line_number, string $line, string $opp): bool` Parse an opp line & set the values used by the differ  
  
  
