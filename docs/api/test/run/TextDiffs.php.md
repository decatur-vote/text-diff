<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/TextDiffs.php  
  
# class DecaturVote\Differ\Test\TextDiffs  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `protected function get_random_str(int $length)`   
- `public function testLargeRandomStringDiffs()`   
- `public function testMultipleDiffs()` Testing a large set of different diff setups  
- `public function testMainDiff()` Standalone test of one input/output  
  
