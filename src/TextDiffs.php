<?php

namespace DecaturVote;

class Differ {

    /**
     * Convert $html into a beautified version with line breaks. Uses taeluf/phtml v0.1.x-dev, which uses DOMDocument
     *
     * @param $html an html string that may not be properly formatted
     * @return an html string that is properly formatted
     */
    public function fix_html(string $html): string{
        return (string)(new \Taeluf\PHTML($html)); 
    }

    /**
     * Convert $old_text into NEW TEXT by applying $opps
     *
     * @param $old_text the old text
     * @param $opps the operations, generated from $this->get_opps($old, $new);
     * @return new text
     */ 
    public function forward(string $old_text, string $opps){
        $opp_lines = explode("\n", $opps);
        $old_lines = explode("\n", $old_text);
        $move_opps = [];
        foreach ($opp_lines as $opp_line){
            $parsed = $this->parse_opp_line($opp_line, $line_number, $line, $opp);
            if ($parsed===false)continue;

            if ($opp == 'add'){
                $old_lines = $this->add_line($old_lines,$line_number, $line);
            } else if ($opp == 'remove'){
                $old_lines = $this->remove_line($old_lines, $line_number);
            } else if ($opp == 'move'){
                $move_opps[] = $opp_line;
            }
        }
        $old_lines = $this->move_lines($old_lines, $move_opps);

        return implode("\n", $old_lines);
    }

    /**
     * Convert $new_text into OLD TEXT by un-applying $opps (essentially runs the opps backward)
     *
     * @param $new_text the new text
     * @param $opps the operations, generated from $this->get_opps($old, $new);
     * @return old text, before the changes
     */ 
    public function backward(string $new_text, string $opps){
        $opp_lines = explode("\n", $opps);
        $opp_lines = array_reverse($opp_lines);
        $new_lines = explode("\n", $new_text);
        $move_opps = [];
        foreach ($opp_lines as $opp_line){
            $parsed = $this->parse_opp_line($opp_line, $line_number, $line, $opp);
            if ($parsed===false)continue;
            if ($opp!='move')continue;
            $move_opps[] = 
                implode(">",
                    array_reverse(
                        explode(">", $opp_line)
                    )
                );
        }

        $new_lines = $this->move_lines($new_lines, $move_opps);

        foreach ($opp_lines as $opp_line){
            $parsed = $this->parse_opp_line($opp_line, $line_number, $line, $opp);
            if ($parsed===false)continue;

            if ($opp == 'remove'){ // we add line, since this is backward
                $new_lines = $this->add_line($new_lines,$line_number, $line);
            } else if ($opp == 'add'){ // we remove line, since this is backward
                $new_lines = $this->remove_line($new_lines, $line_number);
            } else if ($opp == 'move'){
                continue;
            }
        }

        return implode("\n", $new_lines);
    }

    /**
     * Get a string of operations to convert from $old to $new 
     * Allows backward conversions, too.
     *
     * @param $old the old text
     * @param $new the new text
     * @return string of operations to convert from old to new. One operation per line.
     */
    public function get_opps(string $old, string $new): string{
        $lines_old = explode("\n", $old);
        $lines_new = explode("\n", $new);


        // 1. Calculate lines to remove
        $lines_to_remove = [];
        $line_removal_count = 0;
        $lines_to_remove_offsets = [];
        foreach ($lines_old as $line_number => $line){
            if (isset($lines_to_remove_offsets[$line])){
                $offset = $lines_to_remove_offsets[$line];
                $index = array_search(
                    $line,
                    array_slice($lines_new,$offset)
                );
                if ($index!==false)$index += $offset;
            } else {
                $index = array_search($line, $lines_new);
            }

            if ($index!==false){
                $lines_to_remove_offsets[$line] = $index+1;
            }
            if ($index!==false)continue;
            $line_number = $line_number - $line_removal_count;
            $lines_to_remove[] = $line_number.'-'.$line;
            $line_removal_count++;
        }
        unset($lines_to_remove_offsets);

        // 2. Calculate lines to add
        $lines_to_add = [];
        $line_offsets = [];
        foreach ($lines_new as $line_number => $line){
            $offset = $line_offsets[$line] ?? 0;
            $index = array_search($line, array_slice($lines_old,$offset));
            if ($index!==false){
                // if there are multiple occurences of $line in $lines_new
                // then check if $lines_old has all the occurences
                // do this by starting the search at a new location
                //
                // for duplicate lines, increase the offset
                $line_offsets[$line] = $index + 1 + $offset;
                continue;
            }
            $lines_to_add[] = $line_number.'+'.$line;
        }

        unset($line_offsets);


        // 3. Calculate lines to move
        $move_opps = [];
        $pre_move_opps = implode("\n",
            array_merge($lines_to_remove, $lines_to_add)
        );
        $new_text_without_moves = $this->forward($old, $pre_move_opps);
        $new_lines_without_moves = explode("\n", $new_text_without_moves);
        /** @var @key is the string line. @value is the offset this line has been moved to previously. */
        $lines_already_moved_to = [];
        foreach ($new_lines_without_moves as $index => $old_line){
            $new_line = $lines_new[$index];
            if ($old_line==$new_line)continue;

            if (isset($lines_already_moved_to[$old_line])){
                $offset = $lines_already_moved_to[$old_line] + 1;
                search_again_already_set: 
                $new_pos = $offset + array_search($old_line,
                    array_slice($lines_new,$offset)
                );
                $target_line = $new_lines_without_moves[$new_pos];
                if ($old_line == $target_line){
                    $offset = $new_pos + 1;
                    goto search_again_already_set;
                }
            } else {
                $offset = 0;
                search_again:
                $search_in = array_slice($lines_new,$offset);
                $new_pos = array_search($old_line, $search_in) + $offset;
                $target_line = $new_lines_without_moves[$new_pos];

                if ($old_line == $target_line){ 
                    $offset = $new_pos + 1;
                    goto search_again;
                }
            }

            $move_opps[] = $index.'>'.$new_pos;
            $lines_already_moved_to[$old_line] = $new_pos;
        }
        unset($lines_already_moved_to);

        // 4. combine the opps & return them
        $diff_opps = implode("\n",
            array_merge($lines_to_remove, $lines_to_add, $move_opps)
        );

        return $diff_opps;
    }

    /**
     * Add a line to the line buffer & return the corrected lines
     *
     * @param $lines the line buffer array
     * @param $line_number the line number to add the line at
     * @param $new_line the text of the new line to add
     * @return array of lines, with the specified line added. Array indices are updated
     */
    protected function add_line(array $lines, int $line_number, string $new_line): array{
        return  
            array_merge(
                array_slice($lines,0,$line_number),
                [$new_line],
                array_slice($lines,$line_number),
            );
    }

    /**
     * Remove a line from the buffer & return the corrected lines array
     *
     * @param $lines The line buffer array to remove a line from
     * @param $line_number the line number to remove
     * @return an array of lines, with the specified $line_number removed. Array indices are updated.
     */
    protected function remove_line(array $lines, int $line_number): array {
           return 
                array_merge(
                    array_slice($lines,0,$line_number),
                    array_slice($lines,$line_number+1),
                );
    }

    /**
     * Apply move operations to an array of lines
     *
     * @param $lines an array of lines that need moved around 
     * @param $move_opps an array of move operations to perform on the lines
     * @return array of lines, moved to their correct positions
     */
    protected function move_lines(array $lines, array $move_opps){
        $moved_lines = [];
        foreach ($move_opps as $opp_line){
            $parsed = $this->parse_opp_line($opp_line, $old_line_number, $line, $opp);
            $new_line_number = (int)$line;
            $moved_lines[$new_line_number] = $lines[$old_line_number];
        }
        foreach ($moved_lines as $new_index => $line){
            $lines[$new_index] = $line;
        }
        return $lines;
    }

    /**
     * Parse an opp line & set the values used by the differ
     *
     * @param $opp_line, a line like `4+abc` or `5-def` or `6>3`
     * @param $line_number what line number is being modified.
     * @param $line the string text of the line. If MOVE (`6>3`), then this is the line number to move to.
     * @param $opp the operation ('move', 'add', 'remove')
     *
     * @return true if successful parse. false otherwise
     */
    protected function parse_opp_line(string $opp_line, ?int &$line_number, ?string &$line, ?string &$opp): bool{
        if ($opp_line==''){
            return false;
        }
        $char = preg_match('/^[0-9]+([-+>])/', $opp_line, $matches);
        $char = $matches[1];
        $pos = strpos($opp_line, $char);
        $opp_map = [
            '-'=>'remove',
            '+'=>'add',
            '>'=>'move',
        ];
        $opp = $opp_map[$char];
            
        $line_number = (int)substr($opp_line,0,$pos);
        $line = substr($opp_line,$pos+1);

        return true;
    }

}
